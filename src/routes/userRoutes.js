'use strict';

const express = require('express');
const router = express.Router();

const ListAllUsersUseCase = require('../application/use-cases/user/ListAllUsersUseCase');
const Logger = require('../infra/cross-cutting/Logger');
const UserRepository = require('../infra/repositories/UserRepository');
const UserController = require('../controllers/UserController');

//TO-DO: REMOVER INSTÂNCIAS MANUAIS E IMPLEMENTAR MODULO DE SUPORTE A DI
const proxy = { userRepository: new UserRepository(), logger: new Logger() };
const listAllUsersUseCase = new ListAllUsersUseCase({ userRepository: proxy.userRepository });
const userController = new UserController({ listAllUsersUseCase: listAllUsersUseCase, logger: proxy.logger });

/// USER ROUTES ///
router.get('/', userController.getAsync) //list users
    .get('/:id', userController.detailAsync) //get specific user
    .post('/', userController.createAsync)
    .post('/:id/update', userController.updateAsync) //update the data for the specified user
    .post('/:id/delete', userController.deleteAsync); //remove the specified user

module.exports = router;