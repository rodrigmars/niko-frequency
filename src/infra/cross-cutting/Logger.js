module.exports = class Logger {

    constructor() { }

    debug(message) {
        console.log(message);
    }

    info(message) {
        console.log(message);
    }
    
    error(e) {
        console.log(`message: ${e.message} - stack: ${e.stack}`);
    }

}