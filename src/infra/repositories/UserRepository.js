'use strict';
module.exports = class UserRepository {
    
    constructor() {

    }

    async allAsync() {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                try {
                    const users = [{
                        firstName: "Olivia",
                        lastName: "Rebeca Manuela Sales",
                        dateOfBirth: "26/02/1971",
                        email: "oliviarebe@owl-ti.com.br",
                        zipCode: "49067-170",
                        address: "Travessa M"
                    }];

                    // throw new Error('Mensagem de erro teste!');
                    
                    resolve(users);

                } catch (error) {
                    reject(error);
                }
            }, 1000)
        })
    }

}