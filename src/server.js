'use strict';
const express = require('express');
const cors = require('cors');
const helmet = require('helmet');

require('dotenv').config({  
  path: process.env.NODE_ENV === "test" ? ".env.testing" : ".env"
})

const userRoute = require('./routes/userRoutes');

const morgan = require('morgan');

const app = express();

//app.use(morgan("tiny"));
app.use(morgan("combined"));

app.use(helmet());
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/api/v1/users', userRoute);

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => console.log(`server running on port ${PORT}`));