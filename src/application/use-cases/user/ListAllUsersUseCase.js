'use strict';

const EventEmitter = require("events");
module.exports = class UserUseCase extends EventEmitter {

    constructor(proxy) {
        super();
        this.userRepository = proxy.userRepository;
    }

    async executeAsync() {
        await this.userRepository.allAsync()
            .then((users) => {
                this.emit("notify");
                this.emit("success", users);
            })
            .catch((error) => {
                console.log(error);
                this.emit("failure", error);
            })
    }
}