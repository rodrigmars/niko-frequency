'use strict';

module.exports = class UserController {

    constructor(proxy) {
        this.listAllUsersUseCase = proxy.listAllUsersUseCase;
        this.logger = proxy.logger;
    };

    getAsync = async (req, res) => {

        this.listAllUsersUseCase
            .once('notify', () => { this.logger.info("Notifica consulta"); })
            .once('success', (user) => {
                res.send(user);
            })
            .once('failure', (error) => {
                this.logger.error(error);
                res.status(500).send(error.message);
            });

        await this.listAllUsersUseCase.executeAsync();
    };

    detailAsync = async (req, res) => { }

    createAsync = async (req, res) => {

        try {
            let notify = [];

            if (!req.body.fisrtName) {
                return notify.push({
                    success: 'false',
                    message: 'FisrtName is required'
                });
            }

            if (!req.body.lastName) {
                return notify.push({
                    success: 'false',
                    message: 'LastName is required'
                });
            };

            if (!req.body.age) {
                return notify.push({
                    success: 'false',
                    message: 'Age is required'
                });
            };

            if (notify.length)
                return res.status(400).send(notify);

            const user = {
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                age: Number(req.body.age),
                address: ""
            };

            // TO-DO: EXECUTAR AQUI CHAMADA AO BANCO
            return res.status(201).send({
                success: true,
                message: 'User added successfully',
                user
            })

        } catch (e) {
            res.status(500).send({
                success: false,
                message: 'Falha ao cadastrar usuário.'
            });
        }
    };

    updateAsync = async (req, res) => { };

    deleteAsync = async (req, res) => { };
}